%% EMOTION REGULATION IN WM %%
clear all; close all; clc; 
addpath('C:\Program Files\EegLab\eeglab'); addpath('L:\eeglabtools\userfunc'); eeglab; pop_editoptions('option_storedisk', 0); 
%% SETTINGS %% 
do = struct('import',0, 'reimport',0, 'ica',0, 'reica',0, 'epoch',0, 'reepoch',1, 'ersp', 1,  'pac', 0);

% fix - 2000 - image ON - 1500 - circles ON - 1500 - image OFF - 750 - focus ON - 250 focus OFF - 3000

root = 'C:\Users\djole\Downloads\Archive\'; 
% defining the folders Andero says I can have my own system. Thanks!

impdir = [root,'raw\']; implist = dir([impdir, '*.raw']); 
indir = [root,'import\']; mkdir(indir); inlist = dir([indir,'*.set']); 
icadir = [root,'output\ica\']; mkdir(icadir); icalist = dir([icadir, '*.set']);
epochdir = [root,'output\epoch\']; mkdir(epochdir); epochlist = dir([epochdir, '*.set']);
chanlocs = readlocs([root, 'chanlocs.ced'], 'filetype', 'autodetect');

%% IMPORT %%

% importing the data from the datafile to the events. Relabeling the
% channael names and rereferencing. 

while do.import==1; do.import = 0; % update on the cool check list that Andero prepared. 
    
    [trialInfo, trialInfo, trialInfo] = xlsread([root 'test.xlsx']); % uploading the event file. You do this once per loop
    
% we are looping through the participants. 
% I can take the unique subject names from the excel and then match it 

    for s = 1:length(implist); 
        name = implist(s).name(1:end-4); %extract subject name from the file name
        %if ~do.reimport; if any(strcmp([name,'.set'], {inlist(:).name})); continue; end; end                        % overwrite check        
        
        ALLEEG = []; EEG = []; % clearing the eeg space. Erasing anything in the eeg lab. 
        
        % IMPORT %
        EEG = pop_readsegegi('C:\Users\djole\Downloads\Archive\9674277 20160413 0925001.raw'); % brings the data
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'setname','amit_test_raw','gui','off');  % gives the data  a name

        EEG = pop_loadcnt([impdir implist(s).name], 'dataformat', 'auto', 'memmapfile', [indir name '.fdt']);       % import file to EEGLAB
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, CURRENTSET, 'setname', name,'overwrite','on'); 

        % DEFINE CHANNEL LOCATIONS
        EEG.chanlocs = chanlocs;                                                                                    % import channel locations
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); 
        
        % RE-REFERENCE
        [a, refChans] = intersect({EEG.chanlocs.labels},{'TP9', 'TP10'});                                           % find and reference to average mastoids
        EEG = pop_reref(EEG, refChans, 'keepref', 'off');
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); 
        
        % ADD EVENT INFORMATION
        EEG.subject = name;                                                                                         % translate event information                                                                                
        for e = 1:length(EEG.event) 
            if ~strcmp('boundary', EEG.event(e).type)
                EEG.event(e).urtype = EEG.event(e).type;
                [cond,event] = find(str2num(EEG.event(e).type)==ev.code);
                EEG.event(e).urtype = EEG.event(e).type;
                EEG.event(e).cond = ev.cond{cond};
                EEG.event(e).type = ev.event{event};
            end
        end
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);                                               % save to disk
        EEG = pop_saveset(EEG, 'filename', EEG.subject, 'filepath', indir, 'savemode', 'twofiles');
    end
end


%% RUN ICA %%
while do.ica == 1; do.ica = 0;
    inlist = dir([indir , '*.set']); icalist = dir([icadir, '*.set']);                                              % update file lists
    for s = 1:length(inlist);        
        
        if ~do.reica; if any(strmatch(inlist(s).name, {icalist(:).name})); continue; end; end

        ALLEEG = []; EEG = []; CURRENTSET = 1;
        EEG = pop_loadset('filename', inlist(s).name, 'filepath', indir); 
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);

        [ALLEEG EEG CURRENTSET] = pop_copyset(ALLEEG, 1, 2);                                                        % copy a separaate dataset for ICA training
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); 

        EEG = pop_eegfiltnew(EEG, [], 1, 3300, 1, [], 0);                                                           % filter the training data at 1 Hz highpass
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); 

        EEG = pop_epoch(EEG, {'image ON'}, [-2 7], 'epochinfo', 'yes');                                             % extract training epochs 
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); 

        [EEG ALLEEG(1).rejhist.preICA.spec] = pop_rejspec(EEG, 1, [1:EEG.nbchan], -35, 35, 15, 30, 0, 0);           % remove segments with high muscle noise (15-30 Hz)
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);

        EEG = pop_runica(EEG, 'extended', 1, 'interupt', 'on');                                                     % find an ICA solution
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); 
        
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, CURRENTSET, 'retrieve', 1);                               % write the ICA solution back to original data
        EEG = pop_editset(EEG, 'icachansind', 'ALLEEG(2).icachansind', 'icaweights', 'ALLEEG(2).icaweights', 'icasphere', 'ALLEEG(2).icasphere'); 
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);     

        EEG = pop_saveset(EEG, 'filename', EEG.filename, 'filepath', icadir, 'savemode', 'twofiles');
    end
    
end

%% EPOCH AND CLEAN %%
while do.epoch == 1;  do.epoch = 0;
    
    inlist = dir([indir,'*.set']); icalist = dir([icadir,'*.set']); epochlist = dir([epochdir,'*.set']);
    
    segment = {'image ON', [-2 7], [-200 0]};
    epochFilter = 'EEG = pop_eegfiltnew(EEG, 1, 90, 3300, 0, [], 0);';                                          % filtreerimise k�sk
    epochClean = 'EEG = rejSegChan(EEG, eyechans, 100);';                                                       % puhastamisalgoritmi sisend: andmed; kanalid, mida ignoreerida; threshold rejection kriteerium (m�lemale poole 0-ist)
    eyechans = {'VeogU','VeogO'};
    des.cond = {'neutral A' 'neutral B' 'negative high' 'negative low'};                                        % event codes reference structure
    minEpoch = 12;
    bkg = {};
    
    fidd = fopen([epochdir 'params.txt'], 'a+');                                                                % write params
    fprintf(fidd, '\n%s \n%s %s %s \n%s %s \n%s \n%s',num2str(date),...
    ['timelocked to ' segment{1}], ['from ' num2str(segment{2}(1))], ['to ' num2str(segment{2}(2))],...
    ['baseline from ' num2str(segment{3}(1))], ['to' num2str(segment{3}(2))],epochFilter, epochClean);
    fclose(fidd);
        
    [resp, resp, resp] = xlsread([root 'raw\response\collected.xlsx'], 'long');                                 % import response data
    practiceInds = strmatch('PracticeTrials', resp(:,strmatch('Running',resp(1,:))));                           % remove practice trials
    resp = resp(setdiff(1:length(resp),practiceInds),:);
    
    for s = 1:length(icalist); 
        
        if ~do.reepoch; if any(strmatch(icalist(s).name, {epochlist(:).name})); continue; end; end              % �lekirjutuskontroll
        
        ALLEEG = []; EEG = []; CURRENTSET = 0;
        EEG = pop_loadset('filename', icalist(s).name, 'filepath', icadir); 
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
    
        if ~isfield(EEG.event, 'valence')
            respInd = find(str2num(EEG.setname(4:end))==[resp{2:end,strmatch('Subject',resp(1,:),'exact')}])+1;
            eventInd = strmatch('image ON', {EEG.event.type});
            [EEG.event(eventInd).valence] = resp{respInd,strmatch('ValenceRatingSlide.RESP',resp(1,:),'exact')};
            [EEG.event(eventInd).RT] = resp{respInd,strmatch('ValenceRatingSlide.RT',resp(1,:),'exact')};        
            [EEG.event(eventInd).urtypeTest] = resp{respInd,strmatch('NoCirclesMarker',resp(1,:),'exact')};
        
            if ~strcmp([EEG.event(eventInd).urtype],num2str([EEG.event(eventInd).urtypeTest]')')                    % test for file match order
                stop
            end
            [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);

            EEG = pop_saveset(EEG, 'filename', EEG.filename, 'filepath', icadir, 'savemode', 'twofiles');       % salvestab selle ICA kausta tagasi
        end
        
        if isempty(find(EEG.reject.gcompreject))                                                                % vajadusel palub leida silmakomponendid 
            
            EEG = pop_epoch(EEG, segment(1), segment{2}, 'epochinfo', 'yes'); 
            EEG = pop_rmbase(EEG, segment{3});                                                                  % teeb sobivad segmendid
            [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);    
            
            pop_eegplot(EEG, 0, 1, 1); pop_eegplot(EEG, 1, 1, 1);                                               % n�itab jooniseid ja lased valida silmakomponendid
            if length(EEG.reject.gcompreject)>35; pop_selectcomps(EEG, [1:35]); 
            else pop_selectcomps(EEG, [1:length(EEG.reject.gcompreject)]); 
            end; uiwait; close(gcf); close(gcf);
            [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
            gcompreject = EEG.reject.gcompreject;

            if ~isfield(EEG,'IAF') || isempty(EEG.IAF)                                                          % k�sitsi m��ramine: otsi joonisel �les suurim v�i lihtsalt alfam tipp vahemikus 17 (8 Hz) - 27 (13 Hz), pane joonis kinni ja kirjuta tipu x-koordnat j�rgmisena ilmuvasse kasti)
                [a, postChans] = intersect({EEG.chanlocs.labels},{'O1', 'Oz', 'O2', 'POz' 'P3' 'P4'});          % find IAF from posterior electrodes; 
                [spect, sfrqs] = spectopo(EEG.data(postChans,:,:), EEG.pnts, EEG.srate, 'winsize', EEG.srate, 'overlap', EEG.srate/2, 'plot', 'off', 'precent', 100); % 0.5 Hz resolutsiooniga spektrogramm �le tagumiste kanalite
                figure; plot(mean(spect(:,1:find(round(sfrqs*2)/2==20)))); uiwait; IAF = inputdlg; 
                IAF = round(sfrqs(str2num(IAF{:}))*2)/2;
            end
            
            EEG = pop_loadset('filename', icalist(s).name, 'filepath', indir);                                  % faili avamine (icalist nimi, aga indir)
            [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, CURRENTSET, 'overwrite', 'off');             
            
            EEG.rejhist = ALLEEG(1).rejhist;                                                                    % ICA eelne puhastuse ajalukku
            [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
                        
            EEG = pop_editset(EEG, 'icachansind', 'ALLEEG(1).icachansind', 'icaweights', 'ALLEEG(1).icaweights', 'icasphere', 'ALLEEG(1).icasphere'); % kirjutab alleeg(2) andmestiku ICA lahenduse alleeg(1) andmestikku.
            [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);           

            EEG.reject.gcompreject = gcompreject;
            EEG.IAF = IAF;
            [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
                        
            EEG = pop_saveset(EEG, 'filename', EEG.filename, 'filepath', icadir, 'savemode', 'twofiles');       % salvestab selle ICA kausta tagasi
            
            ALLEEG = []; EEG = []; CURRENTSET = 1;
            EEG = pop_loadset('filename', icalist(s).name, 'filepath', icadir);                                 % t�hjendab igaks juhuks ja avab uusti
            [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, CURRENTSET);      
            
        end

        eval(epochFilter);
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); 
        
        EEG = pop_epoch(EEG, segment(1), segment{2}, 'epochinfo', 'yes'); EEG = pop_rmbase(EEG, segment{3});                                 % alles siin n��d saavad defineeritud l�plikud segmendid
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, CURRENTSET, 'overwrite', 'on');                       % salvestab imporditud andmestiku (on vaja kohe �rasalvestada)
        
        EEG = pop_subcomp(EEG, find(EEG.reject.gcompreject), 0);                                                % eemaldab selleks m�rgitud komponendid (esimesena, sest muidu h�sti ei t��ta)
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); 

        eval(epochClean);    
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); % muidu ei salvestu ajalood
        
        bkg = getBkg(EEG, des, bkg, minEpoch, chanlocs);
        
        EEG = pop_interp(EEG, chanlocs, 'spherical');                                                               % intepocholeerib puuduvad kanalid (et maatriksid oleksid sama m��tu)
        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);        
        
        if bkg{end,end}                                                                                         % write to disk only if sufficient number of trials has remained
            EEG = pop_saveset(EEG, 'filename', EEG.subject, 'filepath', epochdir, 'savemode', 'twofiles');     
        end
    end
   
    xlswrite([epochdir date '-export.xlsx'], bkg, 'bkg');                                                        % salvestab meta-data faili
    
    % MAKE STUDY %%
    ALLEEG = []; EEG = []; CURRENTSET = 1;
    epochlist = dir([epochdir,'*.set']); pop_editoptions( 'option_storedisk', 1);
    EEG = pop_loadset('filename',{epochlist.name},'filepath',epochdir);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'study',0); 
    [STUDY ALLEEG] = std_editset( STUDY, ALLEEG, 'name','study','updatedat','off' );
    STUDY = std_makedesign(STUDY, ALLEEG, 1, 'variable1','cond','variable2','','name','cond','pairing1','on','pairing2','on','values1',{'negative high' 'negative low' 'neutral A' 'neutral B'});
    [STUDY EEG] = pop_savestudy( STUDY, EEG, 'filename','study.study','filepath',epochdir);
    CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
    
end

%% IDENTIFY SIGNIFICANCE CLUSTERS %% 
while do.ersp == 1; do.ersp = 0;
    
    % LOAD STUDY AND COMPUTE ERSP %
    epochlist = dir([epochdir,'*.set']); study =  dir([epochdir,'*.study']);
    pop_editoptions('option_storedisk', 1);
    [STUDY ALLEEG] = pop_loadstudy('filename', study(1).name, 'filepath', epochdir);
    CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
    STUDY = std_selectdesign(STUDY, ALLEEG, 1);
    % precompute if needed    
    [STUDY ALLEEG] = std_precomp(STUDY, ALLEEG, {},'interp','on','recompute','off','erp','off','erpparams',{'rmbase' [-200 0] },...
    'ersp','on','erspparams',{'cycles' [3 0.5]  'freqs' [2 80] 'baseline' [0] 'trialbase' 'full' 'freqscale' 'linear' 'timesout' -10, 'padratio' 0.5},'itc','on');

    foi = struct('theta', {[2 6]}, 'alpha', {[8 11]},'beta', {[14 22]});                       % define frequency bands of interest   
    roi = {{'F3' 'Fz' 'FCz' 'F4'}; {'O1' 'O2' 'Oz' 'POz'}};                                                            % define regions of interest

    [D4, D3] = prepERSPeffect(STUDY, ALLEEG, foi);


    % PLOT ERSP % 
    cfg = [];
    cfg.baseline     = [-Inf 0]; 
    cfg.baselinetype = 'absolute'; 
    cfg.showlabels   = 'yes';	
    cfg.colorbar     = 'yes';
    cfg.layout       = D4.layout;
    figure; ft_multiplotTFR(cfg, D4.avg);
    
    % PLOT FREQUENCY
    figure; plot(D4.freq,mean(mean(mean(mean(cat(5,D4.eeglab{:}),5),4),3),2))
    figure; plot(D4.freq,detrend(mean(mean(mean(mean(cat(5,D4.eeglab{:}),5),4),3),2)))
    
    spct = [];
    for i = 1:4;
        spct(:,i) = squeeze(mean(mean(mean(D4.eeglab{i},2),3),4));
    end

    figure; plot(D4.freq,spct)

    
    % JOONISTAB STATISTILISTE EFEKTIDE MAKSIUMUMID JA KESKMISTATUD ERSP
    [stats, df, pval] = statcond(p.rawersp, 'method', 'parametric');                                 % sooritab parameetrilise statistika disianimaatiksile
    fdrp = pval;
    fdrp = fdr(pval);                                                                    % false discovery rate korrektsioon leitud p-v��rtustele
    mp = min(fdrp,[],3);                                                                        % leiame iga TF-punkti jaoks madalaima p-v��rtuse, mis mingis kanalis on leitud
    plp = mp; 
    plp(plp>0.01)=1;                                                              % joonistamiseks v�rdsustame k�ike �ral�ikest suuremad p-v��rtused, et nad oleks joonisel �ht v�rvi
    figure; colormap('gray'); imagesclogy(tms,frqs,plp); set(gca,'YDir','normal');              % draw a figure of significant areas
    std_plottf(tms,frqs,{mean(mean(mean(cat(5,p.rawersp{:}),5),4),3)},'titles', {'AVG'});       % plot average ERSP

    
    
    
    % COMPUTE PREMUTATION-BASED STATISTICS
    cfg = [];
    cfg.channel          = D4.layout.label;                                    % subset to be analyzed (could also be 'all')
    cfg.latency          = [0 7000];
    cfg.frequency        = 'all';
    
    cfg.correctm         = 'cluster';                                       % select the clustering approach
    cfg.method           = 'montecarlo';
    cfg.numrandomization = 500;
    
    cfg.statistic        = 'ft_statfun_depsamplesFmultivariate';            % lower level statistics (used for forming clusters, not corrected for MCP)
    cfg.alpha            = 0.01;
    cfg.tail             = 1;

    cfg.clusterstatistic = 'maxsum';                                        % testing the significance of clusters
    cfg.clusteralpha     = 0.05;
    cfg.clustertail      = 1;
    cfg.correcttail      = 'prob';                                          % see http://www.fieldtriptoolbox.org/faq/why_should_i_use_the_cfg.correcttail_option_when_using_statistics_montecarlo
    cfg.neighbours       = D4.neighbours;
    cfg.minnbchan        = 2;
    
    cfg.uvar             = 1;
    cfg.ivar             = 2;
    cfg.design = []; for sub = 1:size(ersp{1},4); for cond = 1:length(ersp)
        cfg.design(1,end+1) = sub; cfg.design(2,end) = cond; end; end
    
    [stat] = ft_freqstatistics(cfg, d.neutral_A,d.neutral_B,d.negative_low,d.negative_high);
    
    save([epochdir date '-' STUDY.design(STUDY.currentdesign).name '-ft_stats.mat'], 'stat'); 

    
    % plot statistics
    cfg = [];
    cfg.parameter     = 'stat'; % display the statistical value, i.e. the t-score
    cfg.maskparameter  = 'mask';
    cfg.maskalpha = 0.1;
    % select significant cluster mask
    stat.mask = ismember(stat.posclusterslabelmat,find([stat.posclusters.prob] < 0.05));
    cfg.layout       = layout;
    cfg.colorbar     = 'yes';

    figure; ft_multiplotTFR(cfg, stat);
    
    % plot data with statistics mask
    % stat.mask = stat.posclusterslabelmat==3;
    d.stat = d.avg;
    d.stat.powspctrm = squeeze(mean(d.stat.powspctrm,1));
    d.stat.mask = zeros(size(d.stat.powspctrm));
    d.stat.mask(:,end+1-size(stat.mask,2):end,end+1-size(stat.mask,3):end) = stat.mask;     % if baseline period was not included in statistical testing
    d.stat.dimord = 'chan_freq_time';
    cfg = [];
    cfg.maskparameter  = 'mask';
    cfg.maskstyle = 'outline';
    cfg.baseline     = [-Inf 0]; 
    cfg.baselinetype = 'absolute'; 
    cfg.showlabels   = 'yes';	
    cfg.layout       = layout;
    figure; ft_multiplotTFR(cfg, d.stat);

    cfg.channle = {'Oz'};
    figure; ft_singleplotTFR(cfg, d.stat);

    
    ft_clusterplot(cfg, stat);
   
      
    % SALVESTAB SENI ARVUTATUD ANDMED KETTALE 
    % (taaskasutamiseks k�ivita skripti p�is ja ava andmed)
    save([epochdir date '-' STUDY.design(STUDY.currentdesign).name '-data.mat'], 'p'); 

   % SCALP PLOTS %  
    plotERPeffect(pval.alpha,1,{1:4}, 1, 'uncorrected', [2000 4000], [], 'scalpComp')    % plot p-values instead of data; baseline correction usually means 1-p, making the most significant areas the most blue
    
    plotERPeffect(p.beta,1,{1:4}, 1, 'uncorrected', [800 3300], [-200 0], 'scalpDiff')
    plotERPeffect(p.upAlpha,1,{1:4}, 1, 'uncorrected', [3650 4300], [-200 0], 'scalpComp')
 
    % ERP-LIKE JOONISED %
    plotERPeffect(p.alpha,1,{1:3}, 0.05, 'fdr', [-1000 7000], [-2000 0], 'erp') 
    plotERPeffect(p.theta,1,{1:3}, 0.05, 'cluster', [-1000 7000], [-2000 0], 'erp')
    plotERPeffect(p.theta,1,{1:4}, 0.99, 'no', [0 2000], [-1000 0], 'scalpDiff')

    % ANDMETE EKSPORT
    exportERPeffect([epochdir date  '-theta.xlsx'], p.theta,{1:4},...
        {'theta', {'AF3' 'AF4' 'Fz' 'F3' 'F4'}, [2000 4000]}, [-200 0]);                                 

    exportERPeffect([epochdir date  '-beta.xlsx'], p.beta,{1:4},...
        {'beta', {'C3' 'Cz' 'FC1' 'CP1'}, [1000 2500]}, [-200 0]);                                 

end

%% ANALYZE CROSS-FREQUENCY COUPLING %%
while do.pac == 1; do.pac = 0;
end